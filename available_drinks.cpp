#include "available_drinks.h"

AvailableDrinks::AvailableDrinks(QObject *parent)
{
   addItem(new Drink("Caipirinha", 6., cocktail, true));
   addItem(new Drink("Mojito", 6., cocktail, true));
   addItem(new Drink("Mai Tai", 6., cocktail, true));
   addItem(new Drink("Pina Colada", 6., cocktail, true));
   addItem(new Drink("Long Island Iced Tea", 6.5, cocktail, true));
   addItem(new Drink("Canadian Cooler", 6., cocktail, true));
   addItem(new Drink("Bloody Mary", 6., cocktail, true));
   addItem(new Drink("Ipanema", 5., cocktail, false));
   addItem(new Drink("Virgin Mojito", 5., cocktail, false));
   addItem(new Drink("Virgin Pina Colada", 5., cocktail, false));
   addItem(new Drink("KiBa", 5., cocktail, false));

   addItem(new Drink("Cuba Libre", 4.5, longdrink, true));
   addItem(new Drink("Wodka Orange", 4.5, longdrink, true));
   addItem(new Drink("Amaretto Apfel", 4.5, longdrink, true));
   addItem(new Drink("Bacardi Cola", 4.5, longdrink, true));
   addItem(new Drink("Batida Kirsch", 4.5, longdrink, true));

   addItem(new Drink("Wodka", 2., shot, true));
   addItem(new Drink("Tequilla", 2., shot, true));
   addItem(new Drink("Korn", 2., shot, true));
   addItem(new Drink("Bacardi", 2., shot, true));
   addItem(new Drink("Mexikaner", 2., shot, true));

   addItem(new Drink("Pils", 3.5, beer, true));
   addItem(new Drink("Weizen", 3.5, beer, true));
   addItem(new Drink("Guinnes", 3.5, beer, true));
   addItem(new Drink("Desperados", 4., beer, true));
   addItem(new Drink("Pils Alkoholfrei", 3., beer, false));
   addItem(new Drink("Weizen Alkoholfrei", 3., beer, false));

   addItem(new Drink("Cola", 2.5, softdrink, false));
   addItem(new Drink("Fanta", 2.5, softdrink, false));
   addItem(new Drink("Spezi", 2.5, softdrink, false));
   addItem(new Drink("Sprite", 2.5, softdrink, false));
   addItem(new Drink("Wasser Still", 2., softdrink, false));
   addItem(new Drink("Wasser Spritzig", 2., softdrink, false));

}

AvailableDrinks::~AvailableDrinks()
{
    available_drinks.clear();
    filtered_alcoholic_drinks.clear();
    filtered_nonalcoholic_drinks.clear();
}

Q_INVOKABLE bool AvailableDrinks::addItem(Drink *d)
{
//    qDebug()<<"Registering Item"<<d->getName();
    available_drinks.push_back(new DrinkItem(d));
    return true;
}

bool AvailableDrinks::filterItems(QString category){
    filtered_alcoholic_drinks.clear();
    filtered_nonalcoholic_drinks.clear();

    QList<DrinkItem*>::iterator i;
    for (i = available_drinks.begin(); i != available_drinks.end(); i++) {
        if((*i)->getDrink()->getCategoryString() == category){
            if((*i)->getDrink()->getAlcoholic()){
                filtered_alcoholic_drinks.push_back((*i));
            }else{
                filtered_nonalcoholic_drinks.push_back((*i));
            }
        }
    }
    emit filteredDrinksChanged();
    return true;
}
