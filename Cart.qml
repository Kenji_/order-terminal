import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick 2.12
import order_terminal.Cart_Class 1.0

Rectangle {
    height: 150
    width: parent.width
    id: cart_container
    color: "#282828"


    Image {
        id: image
        x: 0
        y: 0
        width: 100
        height: 22
        anchors.horizontalCenter: rectangle.horizontalCenter;
        sourceSize.height: 0
        sourceSize.width: 0
        anchors.bottom: rectangle.top
        anchors.bottomMargin: -2
        fillMode: Image.PreserveAspectFit
        source: "img/expand_button.png"


        TapHandler {
            id: inputHandler
            onTapped: {
                // arbitrary JavaScript expression
                console.log("Expand/Decrease Cart tapped!")
                if(cart_container.height ==150){
                    cart_container.height = cart_container.parent.height
                    cart_item_column.visible = true
                    image.source = "img/decrease_button.png"
                }else{
                    cart_container.height = 150
                    cart_item_column.visible = false
                    image.source = "img/expand_button.png"
                }
            }
        }


    }
    Rectangle {
        id: rectangle
        color: "#404040"
        anchors.top: parent.top
        anchors.topMargin: 35
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0


        ListView {
            id: cart_item_column
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: cart_info_column.top
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            visible: false

            model: Cart_Class.getOrderlist
            delegate:  Item{
                id: element
                height: 25
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                visible: true
                Text{
                    id: itemName
                    color: "white"
                    text: getDrink.getName
                    font.pointSize: 12
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text{
                    id: itemPrice
                    color: "white"
                    text: (getDrink.getPrice*count)+" €"
                    font.pointSize: 12
                    anchors.right: itemAnzahl.left
                    anchors.rightMargin: 20
                    anchors.verticalCenter: parent.verticalCenter
                }
                SpinBox {
                    id: itemAnzahl
                    width: 120
                    height: 20
                    stepSize: 1
                    font.pointSize: 8
                    anchors.rightMargin: 0
                    value: count
                    anchors.right: parent.right;
                    anchors.verticalCenter: parent.verticalCenter
                    onValueChanged:{
                        count=value
                        Cart_Class.calculateTotal();
                        if(value==0){
                            console.log('removing item');
                            Cart_Class.removeItem(getDrink);
                        }
                    }
                }
            }





        }



        Column {
            id: cart_info_column
            transformOrigin: Item.Center
            spacing: 10
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 25

            Text {
                color: "white"
                id: price_element
                text: Cart_Class.total+" €"
                font.pixelSize: 14
                anchors.horizontalCenter: cart_info_column.horizontalCenter;
            }
            Button {
                id: order_button
                text: qsTr("Bestellen")
                font.pointSize: 14
                anchors.horizontalCenter: cart_info_column.horizontalCenter;
                onClicked: {
                     Cart_Class.checkout()
                }
            }
        }

    }
}



/*##^##
Designer {
    D{i:2;anchors_height:100;anchors_width:100}
}
##^##*/
