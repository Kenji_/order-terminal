import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick 2.12
Page {
    anchors.fill: parent
    title:"Glücksrad"

    Rectangle{
        color: "#282828"
        anchors.fill: parent
        id: wheel_of_fortune_container


        Image {
            id: image
            height: 368
            anchors.top: parent.top
            anchors.topMargin: 69
            anchors.right: parent.right
            anchors.rightMargin: 130
            anchors.left: parent.left
            anchors.leftMargin: 130
            fillMode: Image.PreserveAspectFit
            source: "qrc:/img/wheel.png"
        }

        Button {
            id: spin_button
            y: 584
            text: qsTr("Drehen")
            anchors.right: parent.right
            anchors.rightMargin: 115
            anchors.left: parent.left
            anchors.leftMargin: 115
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 16
            onClicked: {
                stackView.push(["Wheel_of_Fortune_Prize_View.qml","Wheel_of_Fortune.qml"])
            }
        }
    }
}





/*##^## Designer {
    D{i:2;anchors_x:19}D{i:3;anchors_width:100;anchors_x:130;anchors_y:27}
}
 ##^##*/
