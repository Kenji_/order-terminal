#ifndef AVAILABLEDRINKS_H
#define AVAILABLEDRINKS_H
#include "drink.h"
#include <QList>
#include <QQmlListProperty>
#include <QRandomGenerator>

class AvailableDrinks : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<DrinkItem> getFilteredAlcoholicDrinks READ getFilteredAlcoholicDrinks NOTIFY filteredDrinksChanged)
    Q_PROPERTY(QQmlListProperty<DrinkItem> getFilteredNonalcoholicDrinks READ getFilteredNonalcoholicDrinks NOTIFY filteredDrinksChanged)

public:
    AvailableDrinks(QObject* parent = 0);
    ~AvailableDrinks();
    Q_INVOKABLE bool filterItems(QString category);

    Q_INVOKABLE QQmlListProperty<DrinkItem> getFilteredAlcoholicDrinks(){
        return QQmlListProperty<DrinkItem>(this, filtered_alcoholic_drinks);
    }
    Q_INVOKABLE QQmlListProperty<DrinkItem> getFilteredNonalcoholicDrinks(){
        return QQmlListProperty<DrinkItem>(this, filtered_nonalcoholic_drinks);
    }
    Q_INVOKABLE Drink* getRandomDrink(){
        return randomDrink;
    }
    Q_INVOKABLE void rollRandomDrink(){

        int randomIndex = QRandomGenerator::global()->bounded(available_drinks.length());
        qDebug()<<"RandomIndex"<<randomIndex;
        randomDrink = available_drinks.at(randomIndex)->getDrink();
    }

private:
    float total;
    bool addItem(Drink* drink);
    QList<DrinkItem*>available_drinks;
    QList<DrinkItem*>filtered_alcoholic_drinks;
    QList<DrinkItem*>filtered_nonalcoholic_drinks;
    Drink* randomDrink;
signals:
    void filteredDrinksChanged();
};

#endif // CART_H
