import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick 2.12
import order_terminal.AvailableDrinks 1.0
import order_terminal.Cart_Class 1.0

Page {
    property string prizeName
    property string prizeCategory
    anchors.fill: parent
    title:"Glücksrad"

    Component.onCompleted: {
        AvailableDrinks.rollRandomDrink();
        let drink = AvailableDrinks.getRandomDrink();
        prizeCategory = drink.getCategoryString;

        if(drink.getName.length>13){
            let whitespaceIndex = drink.getName.lastIndexOf(' ');
            prizeName  = drink.getName.substring(0,whitespaceIndex)+"\n"+drink.getName.substring(whitespaceIndex,drink.getName.length)
        }else{
            prizeName = drink.getName;
        }
    }

    Rectangle{
        color: "#282828"
        anchors.fill: parent
        id: wheel_of_fortune_prize_container


        Button {
            id: spin_again_button
            x: 230
            y: 578
            width: 130
            text: qsTr("Nochmal Drehen")
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 16
            anchors.right: parent.right
            anchors.rightMargin: 32
            onClicked: {
                 stackView.pop()
                 stackView.push(["Wheel_of_Fortune_Prize_View.qml","Wheel_of_Fortune.qml"])
            }

        }

        Button {
            id: take_button
            y: 584
            width: 130
            height: 40
            text: qsTr("Nehm ich")
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 16
            anchors.left: parent.left
            anchors.leftMargin: 32
            onClicked: {
                Cart_Class.addItem(AvailableDrinks.getRandomDrink())
                stackView.pop(null)
            }

        }

        Image {
            id: prize_image
            height: 402
            visible: true
            anchors.top: parent.top
            anchors.topMargin: 70
            anchors.right: parent.right
            anchors.rightMargin: 95
            anchors.left: parent.left
            anchors.leftMargin: 95
            fillMode: Image.PreserveAspectFit
            source: "qrc:/img/"+prizeCategory+"_prize.png"
        }

        Text {
            id: element
            text: prizeName
            font.pixelSize: 26
            anchors.verticalCenter: prize_image.verticalCenter
            anchors.horizontalCenter: prize_image.horizontalCenter
        }


    }
}















/*##^## Designer {
    D{i:2;anchors_x:19}D{i:3;anchors_width:100;anchors_x:130;anchors_y:27}
}
 ##^##*/
