#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "cart.h"
#include "available_drinks.h"
#include <QQuickView>
#include "squircle.h"

using namespace std;

static QObject *available_drinks_singletontype_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return new AvailableDrinks();
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<Drink>("order_terminal.Drink",1,0,"Drink");
    qmlRegisterType<DrinkItem>("order_terminal.DrinkItem",1,0,"DrinkItem");
    qmlRegisterSingletonType<Cart>("order_terminal.Cart_Class", 1, 0, "Cart_Class", &Cart::qmlInstance);
    qmlRegisterSingletonType<AvailableDrinks>("order_terminal.AvailableDrinks", 1, 0, "AvailableDrinks", available_drinks_singletontype_provider);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qmlRegisterType<Squircle>("OpenGLUnderQML", 1, 0, "Squircle");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}
