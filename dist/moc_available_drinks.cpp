/****************************************************************************
** Meta object code from reading C++ file 'available_drinks.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../order_terminal/available_drinks.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'available_drinks.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AvailableDrinks_t {
    QByteArrayData data[11];
    char stringdata0[164];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AvailableDrinks_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AvailableDrinks_t qt_meta_stringdata_AvailableDrinks = {
    {
QT_MOC_LITERAL(0, 0, 15), // "AvailableDrinks"
QT_MOC_LITERAL(1, 16, 22), // "availableDrinksChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 11), // "filterItems"
QT_MOC_LITERAL(4, 52, 8), // "category"
QT_MOC_LITERAL(5, 61, 17), // "getFilteredDrinks"
QT_MOC_LITERAL(6, 79, 27), // "QQmlListProperty<DrinkItem>"
QT_MOC_LITERAL(7, 107, 14), // "getRandomDrink"
QT_MOC_LITERAL(8, 122, 6), // "Drink*"
QT_MOC_LITERAL(9, 129, 15), // "rollRandomDrink"
QT_MOC_LITERAL(10, 145, 18) // "getAvailableDrinks"

    },
    "AvailableDrinks\0availableDrinksChanged\0"
    "\0filterItems\0category\0getFilteredDrinks\0"
    "QQmlListProperty<DrinkItem>\0getRandomDrink\0"
    "Drink*\0rollRandomDrink\0getAvailableDrinks"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AvailableDrinks[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       1,   46, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       3,    1,   40,    2, 0x02 /* Public */,
       5,    0,   43,    2, 0x02 /* Public */,
       7,    0,   44,    2, 0x02 /* Public */,
       9,    0,   45,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // methods: parameters
    QMetaType::Bool, QMetaType::QString,    4,
    0x80000000 | 6,
    0x80000000 | 8,
    QMetaType::Void,

 // properties: name, type, flags
      10, 0x80000000 | 6, 0x00495009,

 // properties: notify_signal_id
       0,

       0        // eod
};

void AvailableDrinks::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<AvailableDrinks *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->availableDrinksChanged(); break;
        case 1: { bool _r = _t->filterItems((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: { QQmlListProperty<DrinkItem> _r = _t->getFilteredDrinks();
            if (_a[0]) *reinterpret_cast< QQmlListProperty<DrinkItem>*>(_a[0]) = std::move(_r); }  break;
        case 3: { Drink* _r = _t->getRandomDrink();
            if (_a[0]) *reinterpret_cast< Drink**>(_a[0]) = std::move(_r); }  break;
        case 4: _t->rollRandomDrink(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (AvailableDrinks::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AvailableDrinks::availableDrinksChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<AvailableDrinks *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlListProperty<DrinkItem>*>(_v) = _t->getFilteredDrinks(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject AvailableDrinks::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_AvailableDrinks.data,
    qt_meta_data_AvailableDrinks,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *AvailableDrinks::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AvailableDrinks::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AvailableDrinks.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int AvailableDrinks::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void AvailableDrinks::availableDrinksChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
