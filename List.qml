import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick 2.12
import order_terminal.Cart_Class 1.0
import order_terminal.AvailableDrinks 1.0

Page {
    property string toolbarTitle
     property string category
    anchors.fill: parent
    title: toolbarTitle

    Component.onCompleted: {
        AvailableDrinks.filterItems(category)
        if(category == "longdrink" || category == "shot"){
            alcoholic_button.visible = false
            nonalcoholic_button.visible = false
            alc_drinks_list.visible = true
            nonalc_drinks_list.visible = false
        }else if(category == "softdrink"){
            alcoholic_button.visible = false
            nonalcoholic_button.visible = false
            alc_drinks_list.visible = false
            nonalc_drinks_list.visible = true
        }else{
            alcoholic_button.visible = true
            nonalcoholic_button.visible = true
            alc_drinks_list.visible = true
            nonalc_drinks_list.visible = false
        }
    }

    Rectangle{
        color: "#282828"
        anchors.fill: parent
        id: drink_list_container

        Button {
            id: alcoholic_button
            anchors.left: parent.left
            anchors.leftMargin: 50
            anchors.top: parent.top
            anchors.topMargin: 15
            height: 50
            width: 150
            text: qsTr("Alkoholisch")
            onClicked: {
                nonalcoholic_button.background.color = "#848484"
                alcoholic_button.background.color = "#B1B1B1"
                alc_drinks_list.visible = true
                nonalc_drinks_list.visible = false
            }
            background: Rectangle {
                radius: 25
                color: "#B1B1B1"
            }
        }

        Button {
            id: nonalcoholic_button
            anchors.left: alcoholic_button.right
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 15
            height: 50
            width: 150
            text: qsTr("Alkoholfrei")
            onClicked: {
                nonalcoholic_button.background.color = "#B1B1B1"
                alcoholic_button.background.color = "#848484"
                alc_drinks_list.visible = false
                nonalc_drinks_list.visible = true
            }
            background: Rectangle {
                radius: 25
                color: "#848484"
            }
        }

        ListView {
            id: alc_drinks_list
            height: 405
            anchors.top: alcoholic_button.bottom
            anchors.right: parent.right
            anchors.rightMargin: 50
            anchors.left: parent.left
            anchors.leftMargin: 50
            visible: true

            model: AvailableDrinks.getFilteredAlcoholicDrinks
            delegate:  Item{
                id: alc_element
                height: 40
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                visible: true
                Text{
                    id: alc_itemName
                    color: "white"
                    text: getDrink.getName
                    font.pointSize: 12
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text{
                    id: alc_itemPrice
                    color: "white"
                    text: (getDrink.getPrice).toFixed(2)+" €"
                    font.pointSize: 12
                    anchors.right: alc_add_drink_to_cart_button.left
                    anchors.rightMargin: 20
                    anchors.verticalCenter: parent.verticalCenter
                }
                Button {
                    id: alc_add_drink_to_cart_button
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    width: 30
                    height: 30
                    text: "+"
                    font.pointSize: 18
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: {
                        Cart_Class.addItem(getDrink)
                    }
                }
            }
        }


        ListView {
            id: nonalc_drinks_list
            height: 405
            anchors.top: alcoholic_button.bottom
            anchors.right: parent.right
            anchors.rightMargin: 50
            anchors.left: parent.left
            anchors.leftMargin: 50
            visible: false

            model: AvailableDrinks.getFilteredNonalcoholicDrinks
            delegate:  Item{
                id: nonalc_element
                height: 40
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                visible: true
                Text{
                    id: nonalc_itemName
                    color: "white"
                    text: getDrink.getName
                    font.pointSize: 12
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text{
                    id: nonalc_itemPrice
                    color: "white"
                    text: (getDrink.getPrice).toFixed(2)+" €"
                    font.pointSize: 12
                    anchors.right: nonalc_add_drink_to_cart_button.left
                    anchors.rightMargin: 20
                    anchors.verticalCenter: parent.verticalCenter
                }
                Button {
                    id: nonalc_add_drink_to_cart_button
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    width: 30
                    height: 30
                    text: "+"
                    font.pointSize: 18
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: {
                        Cart_Class.addItem(getDrink)
                    }
                }
            }


        }
        Cart{
            id: cart_View
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
        }
    }
}
