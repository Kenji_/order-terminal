import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.0
import QtLocation 5.6
import QtQuick 2.9
import QtQuick.Controls 2.2
import order_terminal.AvailableDrinks 1.0

import order_terminal.Cart_Class 1.0


Page {
    id: window
    visible: true
    anchors.fill: parent

    Rectangle{
        color: "#282828"
        anchors.fill: parent

        Column {
                id: column
                height: 470
                width: parent.width
                spacing: 10

                Row {
                    id: row1
                    height: (parent.height / 4) - 10
                    anchors.left: parent.left
                    anchors.leftMargin: 45
                    anchors.right: parent.right
                    anchors.rightMargin: 45
                    spacing: 10

                    Button {
                        id: cocktails_view_button
                        width: parent.width
                        height: parent.height
                        onClicked: {
                            stackView.push("List.qml",{ "category":"cocktail", "toolbarTitle": "Cocktails" })
                            StackView.currentItem
                        }
                        contentItem: Text{
                            text: qsTr("Cocktails")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: cocktails_view_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: parent.height - 20
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/icons/cocktail_icon.png"
                        }
                    }
                }

                Row {
                    id: row2
                    height: (parent.height / 4) - 5
                    anchors.left: parent.left
                    anchors.leftMargin: 45
                    anchors.right: parent.right
                    anchors.rightMargin: 45
                    spacing: 10

                    Button {
                        id: longdrinks_view_button
                        width: (parent.width / 2) - 5
                        height: parent.height
                        onClicked: {
                            stackView.push("List.qml",{ "category":"longdrink", "toolbarTitle": "Longdrinks" })
                            StackView.currentItem
                        }
                        contentItem: Text{
                            text: qsTr("Longdrinks")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            leftPadding: 60
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: longdrinks_view_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: parent.height - 20
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/icons/longdrink_icon.png"
                        }
                    }

                    Button {
                        id: shots_view_button
                        width: (parent.width / 2) - 5
                        height: parent.height
                        onClicked: {
                            stackView.push("List.qml",{ "category":"shot", "toolbarTitle": "Shots" })
                            StackView.currentItem
                        }
                        contentItem: Text{
                            text: qsTr("Shots")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            leftPadding: 60
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: shots_view_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: 70
                            anchors.top: parent.top
                            anchors.topMargin: 20
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/icons/shot_icon.png"
                        }
                    }
                }

                Row {
                    id: row3
                    height: (parent.height / 4) - 5
                    anchors.left: parent.left
                    anchors.leftMargin: 45
                    anchors.right: parent.right
                    anchors.rightMargin: 45
                    spacing: 10

                    Button {
                        id: beers_view_button
                        width: (parent.width / 2) - 5
                        height: parent.height
                        onClicked: {
                            stackView.push("List.qml",{ "category":"beer", "toolbarTitle": "Bier" })
                            StackView.currentItem
                        }
                        contentItem: Text{
                            text: qsTr("Bier")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            leftPadding: 60
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: beers_view_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: parent.height - 20
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/icons/beer_icon.png"
                        }
                    }

                    Button {
                        id: softdrinks_view_button
                        width: (parent.width / 2) - 5
                        height: parent.height
                        onClicked: {
                            stackView.push("List.qml",{ "category":"softdrink", "toolbarTitle": "Softdrinks" })
                            StackView.currentItem
                        }
                        contentItem: Text{
                            text: qsTr("Softdrinks")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            leftPadding: 60
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: softdrinks_view_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: parent.height - 20
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/icons/softdrink_icon.png"
                        }
                    }
                }

                Row {
                    id: row4
                    height: (parent.height / 4) - 10
                    anchors.left: parent.left
                    anchors.leftMargin: 45
                    anchors.right: parent.right
                    anchors.rightMargin: 45
                    spacing: 10

                    Button {
                        id: wheel_of_fortune_button
                        width: parent.width
                        height: parent.height
                        onClicked: {
                            stackView.push("Wheel_of_Fortune_View.qml")
                        }
                        contentItem: Text{
                            text: qsTr("Glücksrad")
                            color: "#FFFFFF"
                            font.pointSize: 26
                            leftPadding: 60
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        background: Rectangle {
                            color: "#282828"
                            border.color: wheel_of_fortune_button.down ? "#FFFFFF" : "#B1B1B1"
                            border.width: 4
                        }
                        Image{
                            height: parent.height - 20
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.left: parent.left
                            anchors.leftMargin: 20

                            fillMode: Image.PreserveAspectFit
                            source: "img/wheel.png"
                        }
                    }
                }
            }

        Cart{
            id: cart_View
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
        }
    }

}

/*##^##
Designer {
    D{i:1;anchors_x:0}
}
##^##*/





/*##^## Designer {
    D{i:4;anchors_width:100;anchors_x:250;anchors_y:312}D{i:3;anchors_x:250;anchors_y:266}
D{i:16;anchors_height:80}D{i:2;anchors_x:250;anchors_y:220}
}
 ##^##*/
