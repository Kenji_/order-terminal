/****************************************************************************
** Meta object code from reading C++ file 'cart.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../order_terminal/cart.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cart.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Cart_t {
    QByteArrayData data[13];
    char stringdata0[142];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Cart_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Cart_t qt_meta_stringdata_Cart = {
    {
QT_MOC_LITERAL(0, 0, 4), // "Cart"
QT_MOC_LITERAL(1, 5, 16), // "orderListChanged"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 12), // "totalChanged"
QT_MOC_LITERAL(4, 36, 7), // "addItem"
QT_MOC_LITERAL(5, 44, 6), // "Drink*"
QT_MOC_LITERAL(6, 51, 10), // "removeItem"
QT_MOC_LITERAL(7, 62, 8), // "getTotal"
QT_MOC_LITERAL(8, 71, 14), // "calculateTotal"
QT_MOC_LITERAL(9, 86, 8), // "checkout"
QT_MOC_LITERAL(10, 95, 12), // "getOrderlist"
QT_MOC_LITERAL(11, 108, 27), // "QQmlListProperty<DrinkItem>"
QT_MOC_LITERAL(12, 136, 5) // "total"

    },
    "Cart\0orderListChanged\0\0totalChanged\0"
    "addItem\0Drink*\0removeItem\0getTotal\0"
    "calculateTotal\0checkout\0getOrderlist\0"
    "QQmlListProperty<DrinkItem>\0total"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Cart[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       2,   66, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       4,    1,   56,    2, 0x02 /* Public */,
       6,    1,   59,    2, 0x02 /* Public */,
       7,    0,   62,    2, 0x02 /* Public */,
       8,    0,   63,    2, 0x02 /* Public */,
       9,    0,   64,    2, 0x02 /* Public */,
      10,    0,   65,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Bool, 0x80000000 | 5,    2,
    QMetaType::Bool, 0x80000000 | 5,    2,
    QMetaType::Float,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 11,

 // properties: name, type, flags
      10, 0x80000000 | 11, 0x00495009,
      12, QMetaType::Float, 0x00495001,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void Cart::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Cart *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->orderListChanged(); break;
        case 1: _t->totalChanged(); break;
        case 2: { bool _r = _t->addItem((*reinterpret_cast< Drink*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 3: { bool _r = _t->removeItem((*reinterpret_cast< Drink*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { float _r = _t->getTotal();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->calculateTotal(); break;
        case 6: _t->checkout(); break;
        case 7: { QQmlListProperty<DrinkItem> _r = _t->getOrderlist();
            if (_a[0]) *reinterpret_cast< QQmlListProperty<DrinkItem>*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Drink* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Drink* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Cart::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Cart::orderListChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Cart::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Cart::totalChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Cart *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlListProperty<DrinkItem>*>(_v) = _t->getOrderlist(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->getTotal(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Cart::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Cart.data,
    qt_meta_data_Cart,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Cart::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Cart::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Cart.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Cart::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Cart::orderListChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Cart::totalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
