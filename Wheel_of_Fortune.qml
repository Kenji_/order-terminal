import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick 2.12
import OpenGLUnderQML 1.0

Page {
    anchors.fill: parent
    background: Rectangle{
        color: "transparent"
    }
    Squircle {
        SequentialAnimation on t {
            NumberAnimation { to: 1; duration: 2500; }
            loops: Animation.Infinite
            running: true
        }
        id: squircled
        onSpinStopped: {
            console.log("QML-Received Stop signal")
            stackView.pop();
        }
    }

}
