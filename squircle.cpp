/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "squircle.h"

#include <QtQuick/qquickwindow.h>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLContext>
#include <QDebug>

#define CLOCKS_PER_SEC ((clock_t) 80000)

//! [7]
Squircle::Squircle()
    : m_t(0)
    , m_renderer(nullptr)
{
    connect(this, &QQuickItem::windowChanged, this, &Squircle::handleWindowChanged);

}
//! [7]

//! [8]
void Squircle::setT(qreal t)
{
    if (t == m_t)
        return;
    m_t = t;
    emit tChanged();
    if (window())
        window()->update();
}
//! [8]

//! [1]
void Squircle::handleWindowChanged(QQuickWindow *win)
{
    if (win) {
        connect(win, &QQuickWindow::beforeSynchronizing, this, &Squircle::sync, Qt::DirectConnection);
        connect(win, &QQuickWindow::sceneGraphInvalidated, this, &Squircle::cleanup, Qt::DirectConnection);
        //! [1]
        // If we allow QML to do the clearing, they would clear what we paint
        // and nothing would show.
        //! [3]
        win->setClearBeforeRendering(false);
    }
}
//! [3]

//! [6]
void Squircle::cleanup()
{
    if (m_renderer) {
        delete m_renderer;
        m_renderer = nullptr;
    }
}
Squircle::~Squircle(){
    qDebug("destruct squircle");
    //m_renderer->glFinish();
    cleanup();
}

SquircleRenderer::~SquircleRenderer()
{
    qDebug("destruct squirclerenderer");
    glClearColor(1., 1., 1., 1);
    glClear(GL_COLOR_BUFFER_BIT);
}
//! [6]

//! [9]
void Squircle::sync()
{
    if (!m_renderer) {
        m_renderer = new SquircleRenderer();
        m_renderer->setupVertexData();
        connect(window(), &QQuickWindow::beforeRendering, m_renderer, &SquircleRenderer::paint, Qt::DirectConnection);
        connect(m_renderer, SIGNAL(sendSpinStopSignal()),this, SIGNAL(spinStopped()));
    }
    m_renderer->setViewportSize(window()->size() * window()->devicePixelRatio());
    m_renderer->setT(m_t);
    m_renderer->setWindow(window());
}
//! [9]



void SquircleRenderer::rotationMatrix(GLfloat angle) {
    GLfloat sinAngle, cosAngle;

    sinAngle = sin(angle * M_PI / 180.0f);
    cosAngle = cos(angle * M_PI / 180.0f);

    for (int i = 0; i < vertexBuffer.size()-11; i = i + 3) {
        float x = vertexBufferCpy[i];
        float y = vertexBufferCpy[i + 1];
        float z = vertexBufferCpy[i + 2];
        vertexBuffer[i] = cosAngle * x - sinAngle * y; // rotated x-value
        vertexBuffer[i + 1] = sinAngle * x + cosAngle * y; // rotated y-value
    }
}


void SquircleRenderer::simulateWheelSpin(){

    if(startspinning){
        float elapsedTime = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        if(elapsedTime>3.3f){
            startspinning = false;
            emit sendSpinStopSignal();
            qDebug("SquircleRenderer: sending Stop animation Signal");
        }else{
            rotationSpeed= rotationSpeed + 5+(1.0f / elapsedTime);
            rotationMatrix(rotationSpeed);
            if(rotationSpeed>360.0)
                rotationSpeed=0.0;
        }

    }
}





//! [4]
void SquircleRenderer::setupVertexData() {


    // Setup circle
    float radius = 0.5;
    vertexBuffer.push_back(0.5);
    vertexBuffer.push_back(0.0);
    vertexBuffer.push_back(0.0);

    int cnt = 0;
    for (float i = 0.0f; i < 2.0f * M_PI; i += 2.0f * M_PI / NUMBER_OF_VERTICES) {
        cnt++;

        vertexBuffer.push_back(cos(i) * radius); //X coordinate
        vertexBuffer.push_back(sin(i) * radius); //Y coordinate
        vertexBuffer.push_back(0.0); //Z coordinate


        if (cnt % 2 == 0) {
            vertexBuffer.push_back(0.0);
            vertexBuffer.push_back(0.0);
            vertexBuffer.push_back(0.0);


            vertexBuffer.push_back(cos(i) * radius); //X coordinate
            vertexBuffer.push_back(sin(i) * radius); //Y coordinate
            vertexBuffer.push_back(0.0); //Z coordinate        //Z coordinate

        }
    }
    vertexBuffer.push_back(0.5);
    vertexBuffer.push_back(0.0);
    vertexBuffer.push_back(0.0);




    // Auswahldreieck
    vertexBuffer.push_back(0.1f);
    vertexBuffer.push_back(0.55f);
    vertexBuffer.push_back(0.0f);


    vertexBuffer.push_back(-0.1f);
    vertexBuffer.push_back(0.55f);
    vertexBuffer.push_back(0.0f);


    vertexBuffer.push_back(0.0f);
    vertexBuffer.push_back(0.4f);
    vertexBuffer.push_back(0.0f);


    vertexBufferCpy = vertexBuffer;
}


void SquircleRenderer::paint()
{
    int uColor=0;

    if (!m_program) {
        initializeOpenGLFunctions();
        m_program = new QOpenGLShaderProgram();
        m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                           "attribute highp vec4 aVertices;"
                                           "attribute highp vec4 aColors;"
                                           "varying highp vec4 vColors;"
                                           "void main() {"
                                           "    gl_Position = aVertices;"
                                           "    vColors= aColors;"
                                           "}");
        m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                           "uniform mediump vec4 color;\n"
                                           "void main(void)\n"
                                           "{\n"
                                           "   gl_FragColor = color;\n"
                                           "}");

        m_program->bindAttributeLocation("aVertices", 0);
        m_program->link();
        startspinning=true;
        start = std::clock();
    }

    m_program->bind();

    m_program->enableAttributeArray(0);
    m_program->setAttributeArray(0, GL_FLOAT, vertexBuffer.data(), 3); //3rd to 0, 4th to 1 by default

    glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());

    glDisable(GL_DEPTH_TEST);

    glClearColor(0.16, 0.16, 0.16, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    simulateWheelSpin();
    //Glücksrad zeichnen
    for (int i = 0; i < NUMBER_OF_VERTICES * 2; i++) {
        if (i % 8 == 0) {

        } else if (i % 8 == 1) {
            glUniform4f(uColor, 0.88f, 0.89f, 0.87f, 1.0f); //grau
            //glUniform4f(uColor, 0.98f, 0.894f, 0.549f, 1.0f); //yellow
        } else if (i % 8 == 2) {
            glUniform4f(uColor, 0.819f, 0.87f, 0.552f, 1.0f); //grün
        } else if (i % 8 == 3) {
            glUniform4f(uColor, 0.819f, 0.87f, 0.552f, 1.0f); //grün
        } else if (i % 8 == 4) {
            glUniform4f(uColor, 0.819f, 0.87f, 0.552f, 1.0f); //grün
        } else if (i % 8 == 5) {
            glUniform4f(uColor, 0.949f, 0.666f, 0.364f, 1.0f); //orange
        } else if (i % 8 == 6) {
            glUniform4f(uColor, 0.88f, 0.89f, 0.87f, 1.0f); //grau
            //glUniform4f(uColor, 0.98f, 0.894f, 0.549f, 1.0f); //yellow
        } else if (i % 8 == 7) {
            glUniform4f(uColor, 0.796f, 0.141f, 0.098f, 1.0f); //red
        }

        glDrawArrays(GL_TRIANGLE_FAN, i, 3);

    }

    glUniform4f(uColor, 0.796f, 0.141f, 0.098f, 1.0f);
    glDrawArrays(GL_TRIANGLE_FAN, 34, 3);


    m_program->disableAttributeArray(0);
    m_program->disableAttributeArray(1);
    m_program->release();
}
