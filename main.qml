import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.0
import QtQuick 2.9
import QtQuick.Controls 2.4


ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 680
    minimumWidth: 640
    minimumHeight: 640
    title: qsTr("Order Terminal")

    header:ToolBar {
        contentHeight: toolButton.implicitHeight
        background:Rectangle{
            anchors.fill: parent
            color: "#282828"
        }

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "-"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                }
            }
            visible: stackView.depth > 1 ? true : false
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
            color: "white"
            font.pointSize: 14
            font.capitalization: Font.Capitalize
        }
    }




    StackView {
        id: stackView
        initialItem: "Home.qml"
        anchors.fill: parent
    }


}

/*##^##
Designer {
    D{i:1;anchors_x:0}
}
##^##*/
