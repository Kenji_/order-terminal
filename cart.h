#ifndef CART_H
#define CART_H
#include "drink.h"
#include <QList>
#include <QQmlListProperty>
class QJSEngine;
class QQmlEngine;

class Cart : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<DrinkItem> getOrderlist READ getOrderlist NOTIFY orderListChanged)
    Q_PROPERTY(float total READ getTotal NOTIFY totalChanged)

public:

    ~Cart();
    static Cart *instance();
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);

    Q_INVOKABLE bool addItem(Drink *);
    Q_INVOKABLE bool removeItem(Drink *);
    Q_INVOKABLE float getTotal();
    Q_INVOKABLE void calculateTotal();
    Q_INVOKABLE void checkout();

    void initTestdata(){
        addItem(new Drink("Caipirinha", 6.0, cocktail, true));
        addItem(new Drink("Caipirinha", 6.0, cocktail, true));
        addItem(new Drink("Mojito", 5., cocktail, true));
    }
    Q_INVOKABLE QQmlListProperty<DrinkItem> getOrderlist(){
        return QQmlListProperty<DrinkItem>(this, order_list);
    }

signals:
    void orderListChanged();
    void totalChanged();
private:
    float total;
    QList<DrinkItem*>order_list;

    explicit Cart(QObject *parent = nullptr);
    static Cart* m_pThis;
};

#endif // CART_H
