#include "cart.h"


Cart* Cart::m_pThis = nullptr;
Cart::Cart(QObject *parent) : QObject(parent)
{
    //initTestdata();
    total = 0.0;
}



Cart::~Cart()
{

}

Q_INVOKABLE bool Cart::addItem(Drink *d1)
{
    qDebug()<<"Adding Item"<<d1->getName();
    bool alreadyInList = false;
    for(int i=0;i<order_list.size();i++){
        if(order_list.at(i)->getDrink()->getName() == d1->getName()){
            alreadyInList = true;
            order_list.at(i)->setCount(order_list.at(i)->getCount()+1);
            break;
        }
    }
    if(!alreadyInList){
        order_list.push_back(new DrinkItem(d1));
    }
    calculateTotal();
    emit orderListChanged();
    return true;
}

Q_INVOKABLE bool Cart::removeItem(Drink *d)
{
    qDebug()<<"Removing Item"<<d->getName();
    for(int i=0;i<order_list.size();i++){
        if(order_list.at(i)->getDrink()->getName() == d->getName()){
            //Check if Count is Over 1
            if(order_list.at(i)->getCount()>1){
                order_list.at(i)->setCount(order_list.at(i)->getCount()-1);
            }else{
                order_list.removeAt(i);
            }
            calculateTotal();
            emit orderListChanged();
            return true;
        }
    }
    return false;
}

Q_INVOKABLE void Cart::calculateTotal(){
    total = 0.00;
    for(int i=0;i<order_list.size();i++) {
        total+=order_list.at(i)->getDrink()->getPrice()*order_list.at(i)->getCount();
    }
    emit totalChanged();
}
Q_INVOKABLE float Cart::getTotal()
{
    return total;
}

Q_INVOKABLE void Cart::checkout()
{
    order_list.clear();
    calculateTotal();
    emit orderListChanged();

}



Cart *Cart::instance()
{
    if (m_pThis == nullptr) // avoid creation of new instances
        m_pThis = new Cart;
    return m_pThis;
}

QObject *Cart::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);
    // C++ and QML instance they are the same instance
    return Cart::instance();
}
