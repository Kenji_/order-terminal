#ifndef DRINK_H
#define DRINK_H
#include <string>
#include <QtCore/QObject>
#include <QString>
#include <QtDebug>
enum Category{cocktail,longdrink,shot,beer,softdrink};
static QString CategoryStrings[] = { "cocktail", "longdrink", "shot", "beer", "softdrink" };

class Drink: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString getCategoryString READ getCategoryString NOTIFY dataChanged)
    Q_PROPERTY(QString getName READ getName NOTIFY dataChanged)
    Q_PROPERTY(float getPrice READ getPrice NOTIFY dataChanged)
    Q_PROPERTY(Category getCategory READ getCategory NOTIFY dataChanged)
    Q_PROPERTY(bool getAlcoholic READ getAlcoholic NOTIFY dataChanged)
public:
    explicit Drink(QObject* parent = 0);
    ~Drink();
    explicit Drink(QString name,float price,Category category,bool alcoholic, QObject* parent = 0);

    QString getName(){ return  name; }
    float getPrice(){ return price; }
    Category getCategory(){ return category; }
    QString getCategoryString(){ qDebug() << "cat"<<CategoryStrings[category]; return CategoryStrings[category]; }
    bool getAlcoholic(){ return alcoholic; }
signals:
    void dataChanged();
private:
    QString name;
    float price;
    Category category;
    bool alcoholic;
};





class DrinkItem: public QObject
{
    Q_OBJECT
    Q_PROPERTY(Drink* getDrink READ getDrink NOTIFY dataChanged)
    Q_PROPERTY(int count READ getCount WRITE setCount NOTIFY dataChanged)
public:
    explicit DrinkItem(QObject* parent = 0){}
    explicit DrinkItem(Drink *drink,QObject* parent = 0){
        this->drink=drink;
        this->count=1;
    }
    ~DrinkItem(){}
    Drink* getDrink(){ return drink; }
    int getCount(){ return count; }
    void setDrink(Drink *drink){ this->drink=drink; }
    void setCount(int count){ this->count=count; emit dataChanged(); }
signals:
    void dataChanged();
private:
    Drink* drink;
    int count;
};

#endif // DRINK_H
